﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using GoogleSheetsToUnity;
using GoogleSheetsToUnity.ThirdPary;
using TinyJSON;
using UnityEngine;
using UnityEngine.Networking;

public delegate void OnSpreedSheetLoaded(GstuSpreadSheet sheet);
public delegate void OnTSVSheetLoaded(List<List<string>> tsvSheet);
namespace GoogleSheetsToUnity
{
    /// <summary>
    /// Partial class for the spreadsheet manager to handle all Public functions
    /// </summary>
    public partial class SpreadsheetManager
    {      
        static GoogleSheetsToUnityConfig _config;
        /// <summary>
        /// Reference to the config for access to the auth details
        /// </summary>
        public static GoogleSheetsToUnityConfig Config
        {
            get
            {
                if (_config == null)
                {
                    _config = (GoogleSheetsToUnityConfig) Resources.Load("GSTU_Config");
                }

                return _config;
            }
            set { _config = value; }
        }
        
        /// <summary>
        /// Read a public accessible spreadsheet
        /// </summary>
        /// <param name="searchDetails"></param>
        /// <param name="callback">event that will fire after reading is complete</param>
        public static void ReadPublicSpreadsheet(GSTU_Search searchDetails, OnSpreedSheetLoaded callback)
        {
            if (string.IsNullOrEmpty(Config.API_Key))
            {
                Debug.Log("Missing API Key, please enter this in the config settings");
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + searchDetails.sheetId);
            sb.Append("/values");
            sb.Append("/" + searchDetails.worksheetName);
            if (searchDetails.hasCells)
                sb.Append("!" + searchDetails.startCell + ":" + searchDetails.endCell);
            sb.Append("?key=" + Config.API_Key);
            
            if (Application.isPlaying)
                new Task(Read(UnityWebRequest.Get(sb.ToString()), searchDetails.titleColumn, searchDetails.titleRow, callback));
#if UNITY_EDITOR
            else
                EditorCoroutineRunner.StartCoroutine(Read(UnityWebRequest.Get(sb.ToString()), searchDetails.titleColumn, searchDetails.titleRow, 
                callback));
#endif
        }
        
        /// <summary>
        /// Wait for the Web request to complete and then process the results
        /// </summary>
        /// <param name="request"></param>
        /// <param name="titleColumn"></param>
        /// <param name="titleRow"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        static IEnumerator Read(UnityWebRequest request, string titleColumn, int titleRow, OnSpreedSheetLoaded callback)
        {
            using (request)
            {
                yield return request.SendWebRequest();

                if (string.IsNullOrEmpty(request.downloadHandler.text) || request.downloadHandler.text == "{}")
                {
                    Debug.LogWarning("Unable to Retrieve data from google sheets");
                    yield break;
                }

                ValueRange rawData = JSON.Load(request.downloadHandler.text).Make<ValueRange>();
                GSTU_SpreadsheetResponce response = new GSTU_SpreadsheetResponce(rawData);

                if (callback != null)
                    callback(new GstuSpreadSheet(response, titleColumn, titleRow));
            }
        }

        /// <summary>
        /// Download a public accessible spreadsheet as tsv format
        /// </summary>
        /// <param name="searchDetails"></param>
        /// <param name="callback">event that will fire after reading is complete</param>
        public static void ReadPublicSpreadsheet(GSTU_Search searchDetails, OnTSVSheetLoaded callback)
        {
            if (string.IsNullOrEmpty(Config.API_Key))
            {
                Debug.Log("Missing API Key, please enter this in the config settings");
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + searchDetails.sheetId);
            sb.Append("/values");
            sb.Append("/" + searchDetails.worksheetName);
            sb.Append("?key=" + Config.API_Key);
            
            if (Application.isPlaying)
                new Task(Read(UnityWebRequest.Get(sb.ToString()), callback));
            #if UNITY_EDITOR
            else
                EditorCoroutineRunner.StartCoroutine(Read(UnityWebRequest.Get(sb.ToString()), callback));
            #endif
        }
        
        /// <summary>
        /// Wait for the Web request to complete and then process the results
        /// </summary>
        /// <param name="request"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        static IEnumerator Read(UnityWebRequest request, OnTSVSheetLoaded callback)
        {
            using (request)
            {
                yield return request.SendWebRequest();

                if (string.IsNullOrEmpty(request.downloadHandler.text) || request.downloadHandler.text == "{}")
                {
                    Debug.LogWarning("Unable to Retrieve data from google sheets");
                    yield break;
                }

                ValueRange rawData = JSON.Load(request.downloadHandler.text).Make<ValueRange>();

                if (callback != null)
                    callback(rawData.values);
            }
        }
        
    }
}